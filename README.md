# Development Docker Containers

A collection of helper scripts, based on [Slack's magic-cli 
tool](https://github.com/slackhq/magic-cli), for working with development
sites in docker containers.

> # magic-cli
> **A foundation for building your own suite of command line tools.**
>
> magic-cli exists to make it easy to create a set of tools that work together.  It's not a tool you use as-is; it's here to offer a starting point for your own custom command line tools.
>
> Learn more about the origins of magic-cli in [The Joy of Internal Tools](https://medium.com/@SlackEng/4a1bb5fe905b), a post on the Slack Engineering blog.

## Overview

DDC was originally built to make it easier to switch between several active projects,
and build sites that can talk to each other. It uses a persistent
[nginx-proxy](https://github.com/jwilder/nginx-proxy) container to allow new
sites to be easily added to the development network. By default it also spins up
a shared MariaDB and Solr container.

## Disclaimer

This code was written for personal development use on a desktop machine running
on a trusted network, and that's the threat model for any security concerns.

Don't use this on a production server.

## Installation
This repository includes a Makefile that will install `ddc` and all of its subcommands into `/usr/local/bin`:

````bash
$ make install
````

You can also use it to uninstall `ddc`:

````bash
$ make uninstall
````

## Initial Setup

### Containers

Once the commands are installed, you will also need to do a one-time install of 
the docker-based system that actually runs the sites. Do do so, first make sure 
you have docker installed, and then run:

```bash
$ ddc install
```

This will create a folder at `~/.ddc` containing the main `docker-compose.yml` 
file, and some folders for storing the Solr and Mysql databases, and the ssl 
certs.

You may want to edit the file to change the default MariaDB Password.

### DNS/hostfile

It is highly recommended to set up a local DNSmasq install to route all 
`*.localhost` requests to localhost.

### SSL Certificates

The [`nginx-proxy`](https://github.com/jwilder/nginx-proxy) container is all 
configured to fully support `https://` sites, you just need to generate some 
SSL certificates and keys, and mark them as trusted:

```bash
$ cd ~/.ddc/certs
$ ddc ssl-key
```

You will then need to mark the generated certificates as trusted. The exact
method to do this will vary by OS, and occasionally by application (eg. Firefox
has it's own certificate store).

## Usage

## General

Running `ddc -h` will list all commands.

### System

To get started, stop any local http, mysql and Solr services, and run:

```bash
$ ddc system start
```

This will spin up the main nginx-proxy, mariadb, and solr containers.

### Databases

#### MariDB / MySQL

  - MariaDB can be accessed normally (port 3306 is shared to the local system).
  - Within your project, the database host is `mysql` instead of `localhost`.

#### Solr

  - Solr collections are stored at ~/.ddc/volumes/solr, and new collections can 
    be added here as normal.
  - Within your project, the solr host is now `solr` instead of `localhost`.

### Working with Sites

Once a site has had it's database settings adjusted, it can be started by 
running the following at from the root of the site:

```bash
$ ddc start -c
```

This spins up a new docker container running the latest node at 
`<foldername>.ddc.localhost`, and gives you a shell in the container's root.
If you exit the shell, the server will still be running, and you can re-connect 
at any time using:

```bash
$ ddc connect
```

If the site root is nested with the project repository, you should start it like
so from the repository root:

```bash
$ ddc start -c -d dist
```

Of course, substitute `dist` with the correct folder name for the actual 
site.

If you don't want to run a seperate shell just for npm/npx commands in the
container, the `ddc nmp` or `ddc npx` command can be used from the same folder
`ddc start` was run in to execute npm/npx commands in the site context. For
example:

```bash
$ ddc npm i
$ ddc npx eslint
```

To stop the site, run the following:

```bash
$ ddc stop
```

This will shut down and destroy the containers.

### Other images

By default, ddc will spin up the latest standard node image. You can change the
tag to use by running, for example:

```bash
$ ddc start -t 8
```

which will start `node:8` instead. You can also change the image entirely:

```bash
$ ddc start -i "ruby"
$ ddc start -i "ruby" -t "2.5"
$ ddc start -i "https://myregistry.example/fancy_custom_image" -w "/var/www/html"
```

## Customization

### Defaults

While DDC is pretty flexible, the current easiest way to change the default
behaviour is to clone the repo and make your own changes to the bash scripts.

### Per-project

To configure a specific project with it's own defaults, use the following
command:

```bash
ddc build-compose
```

The `build-compose` command accepts all the same arguments as `start`, and will
create a `.docker-compose.yml` file which you can edit. `ddc start` will prefer
the `.docker-compose.yml` file if it sees one.

You can also just create a docker compose file from scratch, and as long as you
set the network to the `ddc_default` network, you can leverage the system.

## Updates

A script for updating the tools is also included; it makes installing the latest tools into a single step process:

```bash
$ ddc update
Updated tools to 01ec2ef (2016-03-30 16:20:30 -0700)
```

## Adding commands

When you run `ddc`, it will look for executables in the same directory as itself which have filenames that begin with `ddc-`.
If you wanted to add the command `ddc build`, you would create an executable script called `ddc-build`.

For extra credit, you can add a human-readable description in that list by putting a comment immediately under the `#!` line:

````bash
#!/usr/bin/env bash
# This line will be shown in the list of commands.
````

You can also define any extra parameters that are required for the script with a `# @param` line for each parameter:

````bash
#!/usr/bin/env bash
# This line will be shown in the list of commands.
# @param <command_param> - Longer Description of the Parameter
# @param - If you don’t give a parameter name, a default one will be created for you
````

You scripts needn't be written in bash. For examples, see the install and update
scripts, which are part of the `magic-cli` defaults, and written in Ruby.
