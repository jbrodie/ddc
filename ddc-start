#!/usr/bin/env bash
# Start a docker container to serve the current directory.

siteName=${PWD##*/}
siteFolder="$PWD"
workdir="/usr/src/app"
tag=':latest'
image='node'
envArgs=(-e "HTTPS_METHOD=noredirect")
protocol='http'
connect=0

while getopts ":hd:csi:t:w:e:" opt; do
  case $opt in
    h)
      echo '-h            \tDisplay this help text.'
      echo '-d <directory>\tSubfolder within the repo to serve from.'
      echo '-c            \tAutomatically connect to the started server.'
      echo '-s            \tUse https on the backend server.'
      echo '-i <image>    \tSet the image. Defaults to "node".'
      echo '-t <tag>      \tDocker image tag. Defaults to "latest".'
      echo '-w <workdir>  \tThe workdir of the image. Defaults to "/usr/src/app".'
      echo '-e <variable> \tPass an environment variable to the docker container.'
      exit
      ;;
    d)
      siteFolder="$siteFolder/$OPTARG"
      ;;
    c)
      connect=1
      ;;
    s)
      protocol='https'
      envArgs+=(-e "VIRTUAL_PROTO=https")
      envArgs+=(-e "VIRTUAL_PORT=443")
      ;;
    i)
      image="$OPTARG"
      ;;
    t)
      tag=":$OPTARG"
      ;;
    w)
      workdir="$OPTARG"
      ;;
    e)
      envArgs+=(-e "$OPTARG")
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
  esac
done

shift $((OPTIND-1))

# If the site has a docker-compose.yml, just use that.
if [ -f docker-compose.yml ]; then
  echo "Found docker-compose.yml, ignoring other options."
  docker-compose up -d
else

  docker pull "$image:$tag"
  echo "Starting $siteName at $protocol://$siteName.ddc.localhost"
  docker run -d \
    -h "$siteName.ddc.localhost" \
    --name "$siteName" \
    --network ddc_default \
    --link mysql \
    --link solr \
    -v "$siteFolder":"$workdir" \
    -v "$HOME"/.netrc:/root/.netrc \
    -v "$HOME"/.ssh:/root/.ssh \
    -v "$HOME"/.gitconfig:/root/.gitconfig \
    -v "$HOME"/.gitignore:/root/.gitignore \
    -e "VIRTUAL_HOST=$siteName.ddc.localhost" \
    ${envArgs[@]} \
    "$image:$tag"
fi

# Work-around for a better terminal experience.
# TODO: Create a proper container with an editor experience.
docker exec "$siteName" cp -r /etc/skel/.bashrc /root/
docker exec "$siteName" cp -r /etc/skel/.profile /root/
echo "$siteName running."
echo - run "ech connect for a shell"
echo - run "ech stop" to stop it.

if [ "$connect" == 1 ]; then
  ech connect
fi
